import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IEmployee } from './employee';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private _url: string = "http://dummy.restapiexample.com/api/v1/employees";

  constructor(private http: HttpClient) { }

  getEmployees(): Observable<IEmployee[]>{
    return this.http.get<IEmployee[]>(this._url);
  }

  deleteEmployee(id: number): Observable<void>{
    return this.http.delete<void>("http://dummy.restapiexample.com/api/v1/delete/"+id);
  }

  addEmployee(obj): Observable<IEmployee>{
    console.log(obj);
    return this.http.post<IEmployee>("http://dummy.restapiexample.com/api/v1/create", obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'responseType': 'json'
      })
    });
  }

  getEmployeeByID(id): Observable<IEmployee>{
    return this.http.get<IEmployee>("http://dummy.restapiexample.com/api/v1/employee/"+id);
  }

  updateEmployee(employee): Observable<void>{
    return this.http.put<void>("http://dummy.restapiexample.com/api/v1/update/"+employee.id, employee);
  }
}
