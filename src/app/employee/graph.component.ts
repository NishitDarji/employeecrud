import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Chart } from 'chart.js';


@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit {

  public employees = [];
  name = [];
  salary = [];
  chart = [];
  loading = true;

  constructor(private _employeeService: EmployeeService) { }

  ngOnInit() {
    this._employeeService.getEmployees()
        .subscribe(data => 
          {
            this.employees = data;
            data.forEach(y => {
              this.name.push(y.employee_name);
              this.salary.push(y.employee_salary);
            });
            this.chart = new Chart('canvas', {
              type: 'line',
              data: {
                labels: this.name,
                datasets: [
                  {
                    data: this.salary,
                    borderColor: '#3cba9f',
                    fill: false
                  }
                ]
              },
              options: {
                legend: {
                  display: false
                },
                scales: {
                  xAxes: [{
                    display: true
                  }],
                  yAxes: [{
                    display: true
                  }],
                }
              }
            });
            this.loading = false;
          });
  }

}

 