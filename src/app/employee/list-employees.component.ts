import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-employees',
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.css']
})
export class ListEmployeesComponent implements OnInit {

  angForm: FormGroup;
  public items = [];
  pageOfItems: Array<any>;
  display = 'none';
  employee = {};
  editForm = false;
  loading = true;
  titleOfModal = "";

  constructor(private _employeeService: EmployeeService, private fb: FormBuilder, private router: Router) { 
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      EmployeeName: ['', Validators.required ],
      EmployeeAge: ['', Validators.required ],
      EmployeeSalary: ['', Validators.required],
      ProfileImage: ['']
    });
  }

  ngOnInit() {
    this._employeeService.getEmployees()
        .subscribe(data => {
          this.items = data;
          this.loading = false;
          this.items.sort((a,b) => b.id.localeCompare(a.id));
        });
  }

  onChangePage(pageOfItems: Array<any>){
    this.pageOfItems = pageOfItems;
  }

  
  deleteEmployee(id: number){
    if(confirm("Are you sure to delete this employee?")){
      this._employeeService.deleteEmployee(id)
        .subscribe(s => 
          {
            let index = this.pageOfItems.findIndex(a => a.id == id);
            this.pageOfItems.splice(index,1);
          });
    }
  }

  addEmployee(name, age, salary, profile_image){
    const obj = {
      name,
      age,
      salary,
      profile_image
    }
    this._employeeService.addEmployee(obj)
    .subscribe(s => {
      this.onCloseHandled();
      this.ngOnInit();
      });
  }

  editEmployee(id) {
    this.editForm = true;
    this.titleOfModal = "Edit Employee";
    return this._employeeService.getEmployeeByID(id)
      .subscribe(data => {
        this.employee = data;
        this.openModal();
      });
  }

  updateEmployee(id,name, age, salary, profile_image) {
    const emp = {
      id,
      name,
      age,
      salary,
      profile_image
    }
    return this._employeeService.updateEmployee(emp)
      .subscribe(data => {
        console.log(data);
        this.onCloseHandled();
        let index = this.pageOfItems.findIndex(a => a.id == emp.id);
        this.pageOfItems[index] = {"id":emp.id,"employee_name":emp.name,"employee_age":emp.age,"employee_salary":emp.salary,"profile_image":emp.profile_image };
      });
  }

  openModal(){
    if(!this.editForm){
      this.titleOfModal = "Add Employee";
      this.employee = {};
    }
    this.display='block'; 
  }

  onCloseHandled(){
  this.display='none'; 
  this.editForm=false;
  }


}
